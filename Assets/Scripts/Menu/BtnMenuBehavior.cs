﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BtnMenuBehavior : MonoBehaviour
{
    public BtnMenuManager menuManager;
    public Button btn_menu;
    public int id;
    public bool done;
    public TextMeshProUGUI activityName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (done)
        {
            btn_active();
        }
        else
        {
            btn_unactive();
        }
    }

    public void btn_active()
    {
        btn_menu.interactable = true;
    }

    public void btn_unactive()
    {
        btn_menu.interactable = false;
    }

    
}
