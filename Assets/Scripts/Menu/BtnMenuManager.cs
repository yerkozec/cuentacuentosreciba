﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BtnMenuManager : MonoBehaviour
{
    public List<GameObject> allButton = new List<GameObject>();
    public List<BtnMenuBehavior> allMenuButton = new List<BtnMenuBehavior>();
    public Button btnAllSet;
    // Start is called before the first frame update
    void Start()
    {
        if (VideoManager.done_activitie)
        {
            allMenuButton[0].done = true;
            allMenuButton[1].done = true;
        }
        else
        {
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    public void AllActivitiesAreDoneFor()
    {
        foreach (BtnMenuBehavior bmb in allMenuButton)
        {
            if (bmb.id == 6 && bmb.done)
            {
                btnAllSet.interactable = true;
            }
            else
            {
                string mensaje = string.Format("falta la actividad {0} por ser realizada", bmb.activityName.text);
                Debug.Log(mensaje);
                break;
            }
        }
    }
}
