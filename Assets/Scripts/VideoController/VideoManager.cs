﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    private string status;
    public RawImage videoImage;
    public static bool done_activitie;
    void Start()
    {
        PlayStreamingClip("sting.mp4");
    }

    void OnGUI()
    {
       
    }


    public void PlayStreamingClip(string videoFile, bool looping = false)
    {

        videoPlayer.source = VideoSource.Url;

        videoPlayer.url = Application.streamingAssetsPath + "/" + videoFile;

        videoPlayer.isLooping = looping;

        StartCoroutine(PlayVideo());

    }



    private IEnumerator PlayVideo()
    {

        // We must set the audio before calling Prepare, otherwise it won't play the audio

        var audioSource = videoPlayer.GetComponent<AudioSource>();

        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        videoPlayer.controlledAudioTrackCount = 1;

        videoPlayer.EnableAudioTrack(0, true);

        videoPlayer.SetTargetAudioSource(0, audioSource);



        // Wait until ready

        videoPlayer.Prepare();

        while (!videoPlayer.isPrepared)

            yield return null;



        videoPlayer.Play();
        videoImage.texture = videoPlayer.texture;



        while (videoPlayer.isPlaying)
        {
            done_activitie = true;
            yield return null;
        }


    }

    public void CallPlayVideo()
    {
        StartCoroutine(PlayVideo());
    }

   
}
