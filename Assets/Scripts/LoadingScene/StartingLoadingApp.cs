﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartingLoadingApp : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadApp",3f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadApp()
    {
        SceneManager.LoadScene("ChoosePath");
    }

}
