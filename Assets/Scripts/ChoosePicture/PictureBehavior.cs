﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PictureBehavior : MonoBehaviour
{
    public Image pictureField;
    public PictureManager pictureManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangePicture(Sprite pic)
    {
        pictureField.sprite = pic;
    }

}

[Serializable]
public class PictureToChoose
{
    public int id;
    public Sprite picture;
    public bool active;
}