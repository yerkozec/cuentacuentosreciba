﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberChoosingManager : MonoBehaviour
{
    public TextMeshProUGUI nPicture;
    readonly int minN = 1;
    int maxN;
    public PictureManager pictureManager;
    // Start is called before the first frame update
    void Start()
    {
        maxN = pictureManager.listOfPictures.Count;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeTransectBackward()
    {
        int currentN = Convert.ToInt32(nPicture.text);
        if (currentN == minN)
        {
            nPicture.text = maxN.ToString();
        }
        else
        {
            nPicture.text = (currentN - 1).ToString();
        }
        pictureManager.ChangePicture();
    }

    public void ChangeTransectForward()
    {
        int currentN = Convert.ToInt32(nPicture.text);
        if (currentN == maxN)
        {
            nPicture.text = minN.ToString();
        }
        else
        {
            nPicture.text = (currentN + 1).ToString();
        }
        pictureManager.ChangePicture();
    }
}
