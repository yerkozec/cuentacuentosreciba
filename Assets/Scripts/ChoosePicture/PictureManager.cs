﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class PictureManager : MonoBehaviour
{
    public TextMeshProUGUI pictureNumber;
    public List<PictureToChoose> listOfPictures = new List<PictureToChoose>();
    public PictureBehavior pictureBehavior;
    public static bool done;
    // Start is called before the first frame update
    void Start()
    {
        Directory.CreateDirectory(Application.persistentDataPath + "/picturechosen");
        CheckIfActivityCompleted();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangePicture()
    {
        foreach (PictureToChoose ptc in listOfPictures)
        { 
            int index = Convert.ToInt32(pictureNumber.text);
            if (ptc.id == index)
            {
                pictureBehavior.ChangePicture(ptc.picture);
                ptc.active = true;
            }
            else
            {
                ptc.active = false;
            }
        }
    }

    public void SavePictureDataToPhone()
    {
        PictureChosenAsReference pcr = new PictureChosenAsReference();
        foreach (PictureToChoose ptc in listOfPictures)
        {
            if (ptc.active)
            {
                pcr.id = ptc.id;
            }
        }
        string jsonFile = JsonUtility.ToJson(pcr);
        File.WriteAllText(Application.persistentDataPath + "/picturechosen/pic.json", jsonFile);
    }

    public void CheckIfActivityCompleted()
    {
        if (File.Exists(Application.persistentDataPath + "/picturechosen/pic.json"))
        {
            done = true;
        }
    }
}

[Serializable]
public class PictureChosenAsReference
{
    public int id;
}