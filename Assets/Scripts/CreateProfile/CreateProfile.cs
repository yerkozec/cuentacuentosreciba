﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.IO;
using SimpleJSON;
public class CreateProfile : MonoBehaviour
{
    public ProfileDataInput profileData;
    string path_to_data;
    JSONNode studentData;
    // Start is called before the first frame update
    void Start()
    {
        Directory.CreateDirectory(Application.persistentDataPath + "/profile");
        path_to_data = string.Format(Application.persistentDataPath + "/profile/student.json");
        FillFromDataOfPhone(); 
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SaveDataProfileToPhone()
    {
        ProfileData pd = new ProfileData();
        pd.name = profileData.name.text;
        pd.country = profileData.country.text;
        pd.school = profileData.school.text;
        pd.location = profileData.location.text;
        pd.genre = profileData.genre.value;
        var profile_json = JsonUtility.ToJson(pd);
        File.WriteAllText(path_to_data, profile_json);

        Debug.Log("Datos Guardados");
    }

    public void FillFromDataOfPhone()
    {
        if (File.Exists(path_to_data))
        {
            studentData = JSONNode.Parse(File.ReadAllText(path_to_data));
            profileData.name.text = studentData["name"];
            profileData.country.text = studentData["country"];
            profileData.school.text = studentData["school"];
            profileData.location.text = studentData["location"];
            profileData.genre.value = studentData["genre"];
        }
        else
        {
            Debug.Log("Faltan datos");
        }
    }
}

[System.Serializable]
public class ProfileDataInput
{
    public TMP_InputField name;
    public TMP_InputField country;
    public TMP_InputField school;
    public TMP_InputField location;
    public TMP_Dropdown genre;
}

[Serializable]
public class ProfileData
{
    public string name;
    public string country;
    public string school;
    public string location;
    public int genre;
}