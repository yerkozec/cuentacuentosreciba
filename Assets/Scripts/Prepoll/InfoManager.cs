﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoManager : MonoBehaviour
{
    public GameObject infoField;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TurnOnInfoField()
    {
        infoField.SetActive(true);
    }

    public void TurnOffInfoField()
    {
        infoField.SetActive(false);
    }
}
