﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneController : MonoBehaviour
{
    public string nextSceneName;
    public string prevSceneName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeSceneForward()
    {
        SceneManager.LoadScene(nextSceneName);
    }

    public void ChangeSceneBackward()
    {
        SceneManager.LoadScene(prevSceneName);
    }

    public void ChangeSceneByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName) ;
    }
}
